package com.kan.learn.graphql.login.model;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import com.kan.learn.graphql.login.security.MyPreAuthenticationToken;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * type of MyJWTPreAuthenticationToken factory class
 */
@Component
public class PreAuthenticatedAuthenticationTokenFactory {
	/**
	 * create MyPreAuthenticationToken By MyUserDetails and HttpRequest
	 * @param userDetails 認証ユーザー情報
	 * @param request リクエスト内容
	 * @return MyPreAuthenticationToken
	 */
	public PreAuthenticatedAuthenticationToken create(UserDetails userDetails, HttpServletRequest request) {
		// 前提条件 not null and not empty
		if (Objects.isNull(userDetails)) return  null; // 指定なし
		if (Objects.isNull(request)) return  null; // 指定なし

		// UserDetails and HttpRequest -> PreAuthenticationToken
		return MyPreAuthenticationToken
				.builder()
				.principal(userDetails)
				.details(new WebAuthenticationDetailsSource().buildDetails(request))
				.build()
		;
	}
}

package com.kan.learn.graphql.login.resolver;

import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.kan.learn.graphql.login.exception.MyBadCredentialsException;
import com.kan.learn.graphql.login.model.value.UserId;
import com.kan.learn.graphql.login.service.ProfileService;
import com.kan.learn.graphql.login.service.UserService;
import com.kan.learn.graphql.login.types.CreateProfileInput;
import com.kan.learn.graphql.login.types.CreateUserInput;
import com.kan.learn.graphql.login.types.Profile;
import com.kan.learn.graphql.login.types.User;


/**
 * All Mutation Resolver
 */
@Component
@RequiredArgsConstructor
public class MutationResolver implements GraphQLMutationResolver {
	/*********************************************
	 * filed
	 *********************************************/
	/** services */
	private final UserService userService; // 認証などのユーザーサービス
	private final ProfileService profileService; // プロフィールサービス
	/** 認証用 */
	private final AuthenticationProvider authenticationProvider;
	/*********************************************
	 * Mutations (認証していない人)
	 *********************************************/
	/**
	 * ID・PW ログイン
	 * @param email loginID
	 * @param password loginPW
	 * @return ログインしたユーザー情報
	 */
	@PreAuthorize("isAnonymous()") // UserSerive.isAnonymous にて権限チェック 未ログイン(匿名ユーザー)であれば誰でも
	public User login(String email, String password) {
		// SpringSecurityでの 認証するID・PWの型へ変換
		UsernamePasswordAuthenticationToken credentials = new UsernamePasswordAuthenticationToken(email, password);
		try {
			/**
			 * memo:
			 *  authenticationProvider.authenticate のところでID・PWの検証をしている
			 *      1. implements UserDetailServiceの実装クラスUserService loadUserByUsernameでemailからユーザー(UserDetails)を検索
			 *      2. UserDetailsのパスワード == 暗号化した password であるかチェック
			 *  にて ID・PWの検証実施
			 */
			// ID・PW認証を実行
			SecurityContextHolder.getContext().setAuthentication(this.authenticationProvider.authenticate(credentials));
			// 認証結果正常の場合 認証したユーザーの情報を取得
			return this.userService.getCurrentUser();
		} catch (AuthenticationException ex) {
			// 認証エラーの場合
			throw new MyBadCredentialsException();
		}
	}
	/*********************************************
	 * Mutations (誰でも)
	 *********************************************/
	/**
	 * ユーザーの新規登録
	 * @param createUser 新規登録ユーザー情報
	 * @param createProfile 新規登録ユーザー プロフィール情報
	 * @return 新規登録結果
	 */
	@PreAuthorize("permitAll")
	public User createUser(CreateUserInput createUser, CreateProfileInput createProfile) {
		// まずはユーザー作成 (ユーザーに関連するマスタレコード(ユーザーとプロフィールなどを)新規レコード作成)
		User user = this.userService.create(createUser);

		// プロフィール情報更新
		Profile profile = this.profileService.update(createProfile, user);

		// 更新したプロフィール情報を結合し返却
		user.setProfile(profile);
		return user;
	}

	/**
	 * sample for hasAuthority('ADMIN')
	 * ユーザーのメアド変更(ADMINでなくてもいいのだけれど サンプルとして)
	 * @param userId 対象のユーザーID
	 * @param email 更新メアド
	 * @return 更新後のユーザープロフィール情報
	 */
	@PreAuthorize("hasAuthority('ADMIN')")
	public Profile changeEmail(final String userId, final String email) {
		// 更新対象となるユーザーの メアドを更新
		return this.profileService.changeEmail(email, UserId.of(userId));
	}
}

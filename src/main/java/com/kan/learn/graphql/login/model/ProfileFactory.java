package com.kan.learn.graphql.login.model;

import org.springframework.stereotype.Component;

import com.kan.learn.graphql.login.infrastructure.entity.MUser;
import com.kan.learn.graphql.login.infrastructure.entity.MUserProfile;
import com.kan.learn.graphql.login.types.Profile;

import java.util.Objects;
import java.util.Optional;

/**
 * type of Profile factory class
 */
@Component
public class ProfileFactory {

	/**
	 * create Profile for UserProfile Tables
	 * @param entity record of m_user_profiles
	 * @return Profile result.
	 */
	public Profile create(MUserProfile entity) {
		// 前提条件
		if (Objects.isNull(entity)) return new Profile();
		// ユーザープロフィールからのユーザー情報取得
		Optional<MUser> user = Optional.of(entity.getUser());

		// UserProfile Tables -> Profile
		return Profile
				// build by UserProfile entity...
				.builder()
				// userId
				.id(entity.getUserId())
				// by user
				.nickname(user.map(MUser::getNickname).orElse(null))
				.email(user.map(MUser::getEmail).orElse(null))
				// by user profile
				.familyName(entity.getFamilyName())
				.lastName(entity.getLastName())
				.updateAt(entity.getUpdatedAt()) // use user profile value
				// complete build
				.build()
		;
	}

	/**
	 * MUser Entity -> Profile
	 * @param entity user entity
	 * @return Profile
	 */
	public Profile create(MUser entity) {
		// 前提条件
		if (Objects.isNull(entity)) return new Profile();
		// ユーザーからのユーザープロフィール情報取得
		Optional<MUserProfile> profile = Optional.of(entity.getProfile());

		// UserProfile Tables -> Profile
		return Profile
				// build by UserProfile entity...
				.builder()
				// userId
				.id(entity.getUserId())
				.nickname(entity.getNickname())
				.email(entity.getEmail())
				// by user profile
				.familyName(profile.map(MUserProfile::getFamilyName).orElse(null))
				.lastName(profile.map(MUserProfile::getLastName).orElse(null))
				.updateAt(profile.map(MUserProfile::getUpdatedAt).orElse(entity.getUpdatedAt()))
				// complete build
				.build()
		;
	}
}

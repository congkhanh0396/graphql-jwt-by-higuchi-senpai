package com.kan.learn.graphql.login.security;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * WEB 基本セキュリティ 設定 for SpringSecurity
 */
@Configuration
@EnableWebSecurity // Spring Security 有効
@EnableGlobalMethodSecurity(prePostEnabled = true) // MutationResolverなどのメソッドごとでのセキュリティ設定があるので 有効
@RequiredArgsConstructor
@EnableConfigurationProperties(SecurityProperties.class) // セキュリティ関連の独自 外部設定ファイル値(プロパティ設定値) 有効
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	/*********************************************
	 * Filed
	 *********************************************/
	/** 認証・認可 作業に関する プロバイダー */
	private final AuthenticationProvider authenticationProvider;
	/** JWT認証ヘッダーの解析と認証フィルター */
	private final MyJWTFilter jwtFilter;
	/*********************************************
	 * 設定
	 *********************************************/
	/**
	 * 認証・認可 作業を集約した管理者(AuthenticationManager) に対しての設定
	 * @param auth 設定する 管理者生成器
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) {
		// 独自のPassword Encodeなどを設定する(SecurityConfig参照)するために設定
		auth.authenticationProvider(authenticationProvider);
	}

	/**
	 * WEB Http通信としての基本セキュリティ事項の登録
	 * @param http
	 * @throws Exception
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		http
				.authorizeRequests().anyRequest().permitAll() // 認証済みリクエストの確認 (一旦全ページ)
				.and()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // リクエスト毎に 認証を実施
				.and()
				.csrf().disable() // CSRF 対策機能を無効化
				.addFilterBefore(jwtFilter, RequestHeaderAuthenticationFilter.class); // 共通事前認証filter登録: JWT認証ヘッダーによる認証フィルータの登録
		;
	}

	/**
	 * CORSOrigin 設定
	 * @return CORSOrigin 設定内容
	 */
	@Bean
	public CorsFilter corsFilter() {
		// CORSOrigin
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		// CORSOrigin のどこからでもこいやの雑設定 (※ サンプルなため Hostの制限などはしない 本番環境などでは注意)
		config.addAllowedOrigin(CorsConfiguration.ALL); // どこからでも かかってこいやの雑設定
		config.addAllowedHeader(CorsConfiguration.ALL); // どんな Headerでも一旦認めるぜの雑設定
		config.addAllowedMethod(CorsConfiguration.ALL); // どのリクエストに対しても 一旦認めるぜの雑設定
		// 全てのパターン認める 雑な設定
		source.registerCorsConfiguration("/**", config);
		// CORSOrigin Filterの設定内容 返却
		return new CorsFilter(source);
	}
}

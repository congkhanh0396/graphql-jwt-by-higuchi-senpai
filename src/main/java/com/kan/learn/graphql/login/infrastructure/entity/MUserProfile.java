package com.kan.learn.graphql.login.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.kan.learn.graphql.login.model.value.CreatedAt;
import com.kan.learn.graphql.login.model.value.UpdatedAt;
import com.kan.learn.graphql.login.model.value.UserId;


/**
 * m_user_profiles entity class
 */
@Entity
@Table(name = "m_user_profiles")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MUserProfile {
	/*********************************************
	 * Filed (m_user_profiles columns)
	 *********************************************/
	@Id
	@Getter
	@EmbeddedId
	private UserId userId;
	@Column(name = "family_name")
	private String familyName;
	@Column(name = "last_name")
	private String lastName;
	@Getter
	@Embedded
	private CreatedAt createdAt;
	@Embedded
	private UpdatedAt updatedAt;

	/*********************************************
	 * Filed (relation table)
	 *********************************************/
	@OneToOne(cascade = CascadeType.ALL) // join on m_user_profiles.id = m_users.id.
	@JoinColumn(name = "id", insertable = false, updatable = false) // MUserProfileからの更新は認めない
	private MUser user;
}

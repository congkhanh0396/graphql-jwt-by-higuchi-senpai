package com.kan.learn.graphql.login.types;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CreateUserInput {
	private final String nickname;
	private final String email;
	private final String password;
}

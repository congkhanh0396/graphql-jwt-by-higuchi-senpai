package com.kan.learn.graphql.login.exception;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.kan.learn.graphql.login.model.value.UserId;

import java.text.MessageFormat;

/**
 * domain exception class (ユーザー存在しない例外)
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
@RequiredArgsConstructor
public class UserNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1494317056603130192L;
	/** find user id */
	private final UserId id;

	/**
	 * @return 独自のメッセージ出力
	 */
	@Override
	public String getMessage() {
		return MessageFormat.format("User with ID ''{0}'' isn''t available", id.toString());
	}
}

package com.kan.learn.graphql.login.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import com.kan.learn.graphql.login.exception.UserNotFoundException;
import com.kan.learn.graphql.login.infrastructure.entity.MUser;
import com.kan.learn.graphql.login.infrastructure.entity.MUserProfile;
import com.kan.learn.graphql.login.infrastructure.entity.MUserProfileRepository;
import com.kan.learn.graphql.login.infrastructure.entity.MUserRepository;
import com.kan.learn.graphql.login.model.ProfileFactory;
import com.kan.learn.graphql.login.model.value.UserId;
import com.kan.learn.graphql.login.types.Profile;
import com.kan.learn.graphql.login.types.SaveProfileInput;
import com.kan.learn.graphql.login.types.User;
import static com.kan.learn.graphql.login.util.StreamUtils.toStream;
import javax.transaction.Transactional;


import java.util.List;
import java.util.stream.Collectors;



/**
 * プロフィール系の操作サービス
 */
@Service
@RequiredArgsConstructor
public class ProfileService {
	/*********************************************
	 * filed
	 *********************************************/
	/** 対象のリポジトリ */
	private final MUserRepository userRepository; // user Entity Repository
	private final MUserProfileRepository userProfileRepository; // user profile Entity Repository
	/** 型 生成器 */
	private final ProfileFactory profileFactory;

	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * 指定ユーザーIDで プロフィール情報取得
	 * @param userId 指定ユーザーID
	 * @return 指定ユーザーIDの プロフィール情報
	 */
	public Profile getProfile(final String userId) {
		// to domain UserId type (UserId型へ)
		UserId id = UserId.of(userId);
		// find userId => 見つからない場合はエラー
		return this.profileFactory.create(this.findByUserId(id));
	}

	/**
	 * @return ユーザーのプロフィール 一覧 取得
	 */
	public List<Profile> getProfiles() {
		// ユーザーのプロフィール 一覧を検索し 検索した各結果を 返却値のプロフィール情報へ 変換した形
		return toStream(this.userProfileRepository.findAll())// ユーザーのプロフィール 一覧を検索
			.map(this.profileFactory::create) // 各結果を 返却値のプロフィール情報へ
			.collect(Collectors.toList()) // 変換した 一覧
		;
	}

	/**
	 * ユーザーのプロフィール情報更新
	 * @param profileInput 更新 ユーザーのプロフィール情報
	 * @param user 作成対象となるユーザー
	 * @return 新規に作成した ユーザーのプロフィール情報
	 */
	@Transactional
	public Profile update(final SaveProfileInput profileInput, User user) {
		// 更新対象の決定
		MUserProfile entity = this.findByUserId(user.getId());
		// 更新値の設定
		entity.setFamilyName(profileInput.getFamilyName());
		entity.setLastName(profileInput.getLastName());
		entity.getUpdatedAt().update();

		// ユーザーのプロフィール情報を更新
		return this.profileFactory.create(this.userProfileRepository.saveAndFlush(entity));
	}

	/**
	 * ユーザーのメアドを更新
	 * @param email 更新するメアド
	 * @param userId 更新 ユーザーID
	 * @return 更新後のユーザーのプロフィール情報
	 */
	@Transactional // dynamic update
	public Profile changeEmail(final String email, UserId userId) {
		// 更新する対象の検索
		MUser entity = this.userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
		entity.setEmail(email); // ニックネームの更新
		entity.getUpdatedAt().update(); // 更新日時の更新
		// 更新実行
		this.userRepository.save(entity);

		// 更新後のプロフィール情報返却
		return getProfile(userId.toString());
	}

	/*********************************************
	 * 内部参照可能関数
	 *********************************************/
	/**
	 * ユーザープロフィール情報の検索(By UserId)
	 * @param id UserId
	 * @return 検索結果
	 */
	private MUserProfile findByUserId(final UserId id) {
		return this.userProfileRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
	}
}

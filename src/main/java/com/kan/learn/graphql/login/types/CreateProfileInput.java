package com.kan.learn.graphql.login.types;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CreateProfileInput implements SaveProfileInput {
	private final String familyName;
	private final String lastName;
}

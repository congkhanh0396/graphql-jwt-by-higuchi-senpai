package com.kan.learn.graphql.login.types;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

import com.kan.learn.graphql.login.model.value.UserId;
import com.kan.learn.graphql.login.model.value.UserRoleType;

/**
 * ユーザー 情報
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5579588738943997261L;
	/*********************************************
	 * Filed
	 *********************************************/
	/** ユーザー基本 情報 */
	private UserId id;
	/** ユーザー権限 情報 一覧 */
	@Setter
	private Set<UserRoleType> roleTypes;
	/** ユーザープロフィール情報 */
	@Setter
	private Profile profile;
}

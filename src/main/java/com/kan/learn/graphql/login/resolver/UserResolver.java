package com.kan.learn.graphql.login.resolver;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import com.kan.learn.graphql.login.model.value.UserRoleType;
import com.kan.learn.graphql.login.service.UserService;
import com.kan.learn.graphql.login.types.User;

import java.util.List;
import java.util.stream.Collectors;

import static com.kan.learn.graphql.login.util.StreamUtils.toStream;

/**
 * type User Resolver
 */
@Component
@RequiredArgsConstructor
public class UserResolver implements GraphQLResolver<User> {
	/*********************************************
	 * filed
	 *********************************************/
	/** services */
	private final UserService userService; // 認証などのユーザーサービス

	/*********************************************
	 * type filed Query
	 **********************************************/
	/**
	 * type User.tokenの内容
	 * @param user ユーザー情報
	 * @return type User.token に該当する ユーザー情報から作成したJWT
	 */
	@PreAuthorize("isAuthenticated()") // すでに認証済みであること
	public String getToken(User user) { return this.userService.createJWT(user); }

	/**
	 * type User.rolesの内容
	 * @param user ユーザー情報
	 * @return type User.roles に該当する ユーザー情報の権限一覧
	 */
	public List<String> getRoles(User user) {
		// ユーザー情報の権限一覧 から 該当の形へ変換した リストへ
		return toStream(user.getRoleTypes())
				.map(UserRoleType::toString) // 文字列にした
				.collect(Collectors.toList())
		;
	}
}

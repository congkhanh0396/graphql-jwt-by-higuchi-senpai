package com.kan.learn.graphql.login.model;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.kan.learn.graphql.login.infrastructure.entity.MUser;
import com.kan.learn.graphql.login.model.value.UserRoleType;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import static com.kan.learn.graphql.login.util.StreamUtils.toStream;



/**
 * type of UserDetails factory class
 */
@Component
public class UserDetailsFactory {
	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * create UserDetails for User Tables
	 * @param user record of m_user entity
	 * @return UserDetails
	 */
	public UserDetails create(MUser user) {
		// 前提条件 not null and not empty
		if (Objects.isNull(user)) return  null; // MUser 指定なし
		if (Objects.isNull(user.getRoleTypes()) || user.getRoleTypes().isEmpty()) return null; // 権限値なし

		// User Tables -> UserDetails
		return new User(user.getUserId().toString(), user.getPassword(), toAuthorities(user.getRoleTypes()));
	}

	/*********************************************
	 * 内部参照可能関数
	 *********************************************/
	/**
	 * m_user_roleTypes -> User Authorities on SpringSecurity
	 * @param roleTypes ユーザー権限一覧
	 * @return User Authorities
	 */
	private List<SimpleGrantedAuthority> toAuthorities(Collection<UserRoleType> roleTypes) {
		// ユーザー権限一覧から SpringSecurityのユーザーの持つ権限一覧型へ変換
		return toStream(roleTypes) // 権限情報 一覧 = ユーザー権限から作成
				.map(UserRoleType::toString) // 文字列にした
				.map(SimpleGrantedAuthority::new) // 型を変換した
				.collect(Collectors.toList())
		; // 一覧に変換したもの
	}
}

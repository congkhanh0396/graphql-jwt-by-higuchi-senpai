package com.kan.learn.graphql.login.types;

/**
 * ユーザーのプロフィール 更新値
 */
public interface SaveProfileInput {
	public String getFamilyName();
	public String getLastName();
}

package com.kan.learn.graphql.login.infrastructure.entity;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kan.learn.graphql.login.model.value.UserId;


/**
 * m_user_profiles repository
 */
public interface MUserProfileRepository extends JpaRepository<MUserProfile, UserId> {
}

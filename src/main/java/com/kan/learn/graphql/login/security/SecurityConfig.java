package com.kan.learn.graphql.login.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.kan.learn.graphql.login.service.UserService;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * JWT や Password Encoderなどの認証関連の セキュリティ設定
 */
@Configuration
public class SecurityConfig {
	/**
	 * JSON Web Tokenとなる JSON暗号化 アルゴリズムの設定
	 * @param properties 内部定義 セキュリティプロパティ情報
	 * @return 独自の秘密鍵を定義した 可逆性JSON暗号化 アルゴリズム
	 */
	@Bean
	public Algorithm jwtAlgorithm(SecurityProperties properties) {
		return Algorithm.HMAC256(properties.getTokenSecret());
	}

	/**
	 * JSON Web Token 解析方法の設定
	 * @param properties 内部定義 セキュリティプロパティ情報
	 * @param algorithm 可逆性JSON暗号化 アルゴリズム
	 * @return JSON Web Token 解析方法
	 */
	@Bean
	public JWTVerifier verifier(SecurityProperties properties, Algorithm algorithm) {
		return JWT
				.require(algorithm) // 暗号化アルゴリズムの設定
				.withIssuer(properties.getTokenIssuer()) // JWT発行者の設定
				.build();
	}

	/**
	 * PasswordEncoder の 暗号化方法を登録
	 * @param properties 内部定義 セキュリティプロパティ情報
	 * @return 独自の暗号化方法(Encode)を定義した PasswordEncoder
	 */
	@Bean
	public PasswordEncoder passwordEncoder(SecurityProperties properties) {
		// BCryptによる指定ストレッチ回数でのハッシュ暗号化
		return new BCryptPasswordEncoder(properties.getPasswordStrength());
	}

	/**
	 * ユーザー認証方法の登録
	 * @param myUserService 独自のユーザー認証認可サービスを実装したサービス
	 * @param passwordEncoder 独自の設定を施した PasswordEncoder
	 * @return ユーザー認証方法の登録 後の認証・認可 作業に関する プロバイダー
	 */
	// ユーザー認証方法(implements UserDetailsService)と PWのエンコード方法アルゴリズムなどを登録(DBアクセスかつトランザクション管理するのでDaoAuthenticationProviderかと)
	@Bean
	public AuthenticationProvider authenticationProvider(UserService myUserService, PasswordEncoder passwordEncoder) {
		// DB access for login auth ... etc のため DBアクセスでの認証器で初期生成
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		// 作業内容の登録
		provider.setUserDetailsService(myUserService); // ID/PW認証などの詳細独自サービスの設定
		provider.setPasswordEncoder(passwordEncoder); // PW 暗号化方法の設定
		// 結果返却
		return provider;
	}
}

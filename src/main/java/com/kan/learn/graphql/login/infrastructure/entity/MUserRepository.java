package com.kan.learn.graphql.login.infrastructure.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.kan.learn.graphql.login.model.value.UserId;

import java.util.Optional;

/**
 * m_user repository
 */
public interface MUserRepository extends JpaRepository<MUser, UserId> {
	/**
	 * Email から 該当のユーザー検索
	 * @param email Email
	 * @return 該当ユーザー
	 */
	public Optional<MUser> findByEmail(String email);

	/**
	 * 指定Email のユーザ存在確認 (重複確認用)
	 * @param email Email
	 * @return true:exist / false: not exist
	 */
	public boolean existsByEmail(String email);

	/**
	 * @return ユーザーIDのふり番
	 */
	@Query(value = "SELECT nextval('user_id_sequence')", nativeQuery = true)
	public Long nextUserId();
}

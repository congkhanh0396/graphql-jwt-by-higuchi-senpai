package com.kan.learn.graphql.login.types;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import java.io.Serializable;

import com.kan.learn.graphql.login.model.value.UpdatedAt;
import com.kan.learn.graphql.login.model.value.UserId;

/**
 * ユーザー プロフィール情報
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Profile implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2034360487102731453L;
	/*********************************************
	 * Filed
	 *********************************************/
	private UserId id;
	@Setter
	private String nickname;
	@Setter
	private String email;
	@Setter
	private String familyName;
	@Setter
	private String lastName;
	private UpdatedAt updateAt;
}

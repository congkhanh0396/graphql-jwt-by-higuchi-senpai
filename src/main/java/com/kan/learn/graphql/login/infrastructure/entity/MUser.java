package com.kan.learn.graphql.login.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;


import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.kan.learn.graphql.login.model.value.CreatedAt;
import com.kan.learn.graphql.login.model.value.UpdatedAt;
import com.kan.learn.graphql.login.model.value.UserId;
import com.kan.learn.graphql.login.model.value.UserRoleType;

import java.util.Set;

/**
 * m_users entity class
 */
@Entity(name = "user")
@Table(name = "m_users")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MUser {
	/*********************************************
	 * Filed (m_users columns)
	 *********************************************/
	@Id
	@Getter
	@EmbeddedId
	private UserId userId;
	private String nickname;
	@Column(unique = true)
	private String email;
	private String password;
	@Getter
	@Embedded
	private CreatedAt createdAt;
	@Embedded
	private UpdatedAt updatedAt;

	/*********************************************
	 * Filed (relation table)
	 *********************************************/
	/** 権限情報 */
	@ElementCollection // m_user_roleの該当ユーザーの権限を一覧で取得
	@CollectionTable(name = "m_user_roles", joinColumns = @JoinColumn(name = "id")) // join on m_user.id = m_user_role.id
	@Embedded
	private Set<UserRoleType> roleTypes;
	/** プロフィール情報(MUserからProfileを検索するパターン) */
	@OneToOne(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true) // join on m_user.id = m_users_profile.id.
	@JoinColumn(name = "id", referencedColumnName = "id", updatable = false) // MUserからの更新はしない
	private MUserProfile profile;
}
